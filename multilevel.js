
/**
 * JavaScript behaviors for the front-end display of webforms.
 */
(function ($) {

$(function() {
  Drupal.webform.multilevelSelectbox();
});

Drupal.webform = Drupal.webform || {};

Drupal.webform.multilevelSelectbox = function(context) {
  $('.webform-multilevel-selectbox').each(function() {
    var data = eval('(' + $(this).children('span').html() + ')');
    $(this).children('span').remove();

    var input =  $(this).children('input')[0];

    var selectedCategories = $(input).val().split("|");
    removeBlank(selectedCategories);
    console.log(selectedCategories);

    var selectboxContainer = $('<div />')[0];
    $(this).append(selectboxContainer);

    var selectboxStack = [];

    updateUI();
    // buildSelectbox(data.items, 0);
    // buildUI(); 
    // changeHandler({ target: selectboxStack[0] });
    $(this).css('display', 'block');


    /*
     * Helpers
     */
    function getLevel(select) {
      for (var i=0;i<selectboxStack.length;i++) {
        if (selectboxStack[i] == select) {
          return i;
        }
      }

      throw "Cannot find the select";
    }

    function removeBlank(array) {
      for (var i=array.length - 1;i>=0;i--) {
        if (array[i] == "") array.splice(i, 1);
      }
    }

    function buildSelectbox(items, level) {
      var size = 0;
      for (var k in items) size++;

      if (size == 0) return;

      var select = $('<select />')[0];
      selectboxStack.push(select);

      var is_selected = (level >= selectedCategories.length) ? 'selected="selected"' : '';

      $(select).append($('<option value="" ' + is_selected + '>-- Please select --</option>'));
      select.items = items;
      for (var k in select.items) {
        is_selected = (level < selectedCategories.length && k == selectedCategories[level]) ? 'selected="selected"' : '';
        console.log(level + " " + is_selected + " " + k + " " + selectedCategories[level]);
        $(select).append($('<option value="' + k + '" ' + is_selected + '>' + k + '</option>'));
      }
    }

    function changeHandler(event) {
      var select = event.target;
      var index = $(select).children(':selected').index();
      var level = getLevel(select);

      while (level < selectedCategories.length) selectedCategories.pop();
      if (index > 0) selectedCategories.push($(select).val());

      while ((level+1) < selectboxStack.length) selectboxStack.pop();
      $(input).val(selectedCategories.join('|'));
      
      updateUI(); 
    }

    function updateUI(force) {
      $(selectboxContainer).html('');

      var last_index = selectboxStack.length - 1;
      var current_data = (last_index == -1) ? data.items : selectboxStack[last_index].items[selectedCategories[last_index]];
      for (var i=selectboxStack.length;i<selectedCategories.length;i++) {
        buildSelectbox(current_data, i);
        current_data = current_data[selectedCategories[i]];
      }

      buildSelectbox(current_data, selectedCategories.length);

      for (var i=0;i<selectboxStack.length;i++) {
        var div = $('<div class="control-group">');
        $(div).append('<label class="control-label">Level ' + (i+1) + ':</label>');

        var innerDiv = $('<div class="controls">');
        $(innerDiv).append(selectboxStack[i]);
        $(div).append(innerDiv);

        $(selectboxContainer).append(div);

        $(selectboxStack[i]).change(changeHandler);
      }
    }
  });
}

})(jQuery);
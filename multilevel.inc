<?php

/**
 * @file
 * Webform module multilevel component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_multilevel() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'mandatory' => 0,
    'value' => '',
    'extra' => array(
      'items' => _webform_expand_items_multilevel(array(
        'Option 1' => array('Option 1.1' => array(), 'Option 1.2' => array(), 'Option 1.3' => array()),
        'Option 2' => array('Option 2.1' => array('Option 2.1.1' => array()))
       )),
      'field_prefix' => '',
      'field_suffix' => '',
      'disabled' => 0,
      'title_display' => 0,
      'description' => '',
      'attributes' => array(),
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_multilevel() {
  return array(
    'webform_display_multilevel' => array(
      'render element' => 'element',
      'file' => 'components/multilevel.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_multilevel($component) {
  $form = array();
  $form['extra']['items'] = array(
    '#type' => 'textarea',
    '#title' => t('Items'),
    '#default_value' => _webform_expand_items_multilevel(_webform_items_data_to_json_multilevel($component['extra']['items'])),
    '#description' => t('Items of this multi-level selectbox'),
    '#cols' => 60,
    '#rows' => 15,
    '#weight' => 0,
    '#required' => TRUE,
    '#wysiwyg' => FALSE,
    '#parents' => array('extra', 'items'),
  );
  $form['display']['field_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix text placed to the left of the multilevel'),
    '#default_value' => $component['extra']['field_prefix'],
    '#description' => t('Examples: $, #, -.'),
    '#size' => 20,
    '#maxlength' => 127,
    '#weight' => 1.1,
    '#parents' => array('extra', 'field_prefix'),
  );
  $form['display']['field_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Postfix text placed to the right of the multilevel'),
    '#default_value' => $component['extra']['field_suffix'],
    '#description' => t('Examples: lb, kg, %.'),
    '#size' => 20,
    '#maxlength' => 127,
    '#weight' => 1.2,
    '#parents' => array('extra', 'field_suffix'),
  );
  $form['display']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#return_value' => 1,
    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
    '#weight' => 11,
    '#default_value' => $component['extra']['disabled'],
    '#parents' => array('extra', 'disabled'),
  );
  return $form;
}

function _webform_expand_items_multilevel($json) {
  $rows = _webform_expand_items_multilevel_internally($json);

  return join("\n",$rows);
}

function _webform_expand_items_multilevel_internally($options) {
  if (!$options || count($options) == 0) return array();

  $items = array();

  foreach ($options as $key => $value) {
    array_push($items, $key);

    $children = _webform_expand_items_multilevel_internally($value);

    foreach ($children as $child) {
      array_push($items, $key . "|" . $child);
    }
  }

  return $items;
}

function _webform_items_data_to_json_multilevel($raw_data) {
  $json = array();
  $rows = explode("\n", $raw_data);

  foreach ($rows as $line) {
    $line = trim($line);

    if ($line == '') continue;

    $tokens = explode("|", $line);
    $level = &$json;
    for ($i=0;$i<count($tokens);$i++) {
      $key = $tokens[$i];

      if (!isset($level[$key])) $level[$key] = array();

      $level = &$level[$key];
    }
  }

  return $json;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_multilevel($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $data = array(
    'name' => 'submitted[' . $component['form_key'] . ']',
    'items' => _webform_items_data_to_json_multilevel($component['extra']['items']),
  );

  $element = array(
    '#type' => 'markup',
    '#input' => TRUE,
    '#weight' => $component['weight'],
    '#markup' => '<div class="control-group webform-multilevel-selectbox" style="display: none;">' 
                  . '<input type="text" name="submitted[' . $component['form_key'] . ']" value="' . $value[0] . '" style="display: none;">'
                  . '<span>'
                  . json_encode($data) 
                  . '</span>'
                  . '</div>',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'multilevel') . '/multilevel.js'),
    ),
  );
  
  if (isset($value)) {
    $element['#default_value'] = $value[0];
  }

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_multilevel($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#field_prefix' => $component['extra']['field_prefix'],
    '#field_suffix' => $component['extra']['field_suffix'],
    '#format' => $format,
    '#value' => isset($value[0]) ? $value[0] : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}


/**
 * Format the output of data for this component.
 */
function theme_webform_display_multilevel($variables) {
  $element = $variables['element'];
  $prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
  $suffix = $element['#format'] == 'html' ? '' : $element['#field_suffix'];
  $value = $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];
  return $value !== '' ? ($prefix . $value . $suffix) : ' ';
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_multilevel($component, $sids = array()) {
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;
  $wordcount = 0;

  $result = $query->execute();
  foreach ($result as $data) {
    if (drupal_strlen(trim($data['data'])) > 0) {
      $nonblanks++;
      $wordcount += str_word_count(trim($data['data']));
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);
  $rows[2] = array(t('Average submission length in words (ex blanks)'), ($nonblanks != 0 ? number_format($wordcount/$nonblanks, 2) : '0'));
  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_multilevel($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_multilevel($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_multilevel($component, $export_options, $value) {
  return !isset($value[0]) ? '' : $value[0];
}
